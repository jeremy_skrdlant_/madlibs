//
//  ContentView.swift
//  Mad Libs
//
//  Created by Jeremy Skrdlant on 8/20/20.
//  Copyright © 2020 NWKTC. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
