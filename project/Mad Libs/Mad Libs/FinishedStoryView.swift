//
//  FinishedStoryView.swift
//  Mad Libs
//
//  Created by Jeremy Skrdlant on 8/20/20.
//  Copyright © 2020 NWKTC. All rights reserved.
//

import SwiftUI

struct FinishedStoryView: View {
    var body: some View {
        Text("A [NOUN] in [WACKY STATE] was arrested this morning after he [VERB] a [NOUN] in front of [NOUN].  [PROPER NAME], had a history of [VERB], but no one, not even his [NOUN], ever imagined he'd [VERB] with a [NOUN].")
    }
}

struct FinishedStoryView_Previews: PreviewProvider {
    static var previews: some View {
        FinishedStoryView()
    }
}
